let posts = []
let count = 1

const showPosts = (posts) => {
	let post_entries = ''
	posts.forEach((post) => {
		post_entries += `
			<div id="post-${post.id}">
			    <h3 id="post-title-${post.id}">${post.title}</h3>
			    <p id="post-body-${post.id}">${post.body}</p>
			    <button onclick="editPost('${post.id}')">Edit</button>
			    <button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	document.querySelector('#div-post-entries').innerHTML = post_entries
}

//add new post
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	event.preventDefault()
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})
	//For incrementing the ID to be unique for each new post
	count++

	//For showing the updated posts after adding a new post
	showPosts(posts)

	alert('Post added successfully!')
})

//edit post function
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}

//updating a post
document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
    event.preventDefault();

    for (let i = 0; i < posts.length; i++) {
        // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first.

        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;
    
            showPosts(posts);
            alert('Successfully updated.');
            
            break;
        }
    }
});

//delete post
const deletePost = (id) => {
	let index = posts.findIndex(obj => obj.id == id)
	const message = 'Click ok to confirm.'
	if(confirm(message) == true){
		posts.splice(index, 1)
		showPosts(posts)
	}	
}